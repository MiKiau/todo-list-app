import {StyleSheet} from "react-native";
import TodoList from "../models/TodoList";
import {ListItem} from "react-native-elements";
import React from "react";

interface ListDisplayProps {
  list: TodoList;
  navigation: any;
}

export default function TodoListContainer({list, navigation}: ListDisplayProps) {
  const checkedItems = list.items.filter((item: any) => item.checked).length;
  return (
    <ListItem
      style={styles.container}
      onPress={() => navigation.navigate("list-view", { listTitle: list?.title })}
    >
      <ListItem.Content>
        <ListItem.Title>
          {list.title}
        </ListItem.Title>
        <ListItem.Subtitle>
          ({checkedItems} completed out of {list.items.length})
        </ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron/>
    </ListItem>
  )
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
  }
});