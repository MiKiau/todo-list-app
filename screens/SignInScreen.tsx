import {StyleSheet, Text, View} from "react-native";
import {Input, Button} from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";
import React, { useState } from "react";
import {getAuth, signInWithEmailAndPassword} from 'firebase/auth';
import {StackScreenProps} from "@react-navigation/stack";
import UserAuthentication from "../models/UserAuthentication";

const auth = getAuth();

const SignInScreen: React.FC<StackScreenProps<any>> = ({ navigation }) =>  {
  const [value, setValue] = useState<UserAuthentication>(getInitialAuthentication());

  // function toHomePage() {
  //   navigation.navigate('home')
  // }
  async function login() {
    if (value.email === '' || value.password === '') {
      setValue({
        ...value,
        error: 'Email and password are mandatory.'
      })
      return;
    }

    try {
      await signInWithEmailAndPassword(auth, value.email.toLocaleLowerCase(), value.password);
    } catch (error: any) {
      setValue({
        ...value,
        error: "Email or password is incorrect.",
      })
    }
  }
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <Text style={styles.title}>Simple check list</Text>
        <View style={styles.buttons}>
          <Input
            placeholder='Email'
            containerStyle={styles.control}
            value={value.email}
            onChangeText={(text) => setValue({...value, email: text})}
            leftIcon={<Icon name='envelope' size={16}/>}
          />
          <Input
            placeholder='Password'
            containerStyle={styles.control}
            value={value.password}
            onChangeText={(text) => setValue({...value, password: text})}
            secureTextEntry={true}
            leftIcon={<Icon name='key' size={16}/>}
          />
        </View>
        <View style={styles.buttons}>
          {!!value.error && <View style={styles.error}><Text>{value.error}</Text></View>}
          <Button
            onPress={login}
            title={"Login"}
          />
          {/*<Button*/}
          {/*  type={"outline"}*/}
          {/*  title={"Without login"}*/}
          {/*  onPress={toHomePage}*/}
          {/*/>*/}
          <Text onPress={() => navigation.navigate("sign-up")}>
            Create a new account
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    padding: 20,
  },
  main: {
    flex: 1,
    justifyContent: "center",
    maxWidth: 960,
    marginHorizontal: "auto",
    gap: 5,
  },
  buttons: {
    gap: 15,
  },
  control: {
    width: "100%",
  },
  title: {
    fontSize: 64,
    fontWeight: "bold",
  },
  subtitle: {
    fontSize: 36,
    color: "#38434D",
  },
  error: {
    marginTop: 10,
    padding: 10,
    color: '#fff',
    backgroundColor: '#D54826FF',
  }
});

export function getInitialAuthentication(): UserAuthentication {
  return {
    email: '',
    password: '',
    error: ''
  };
}

export default SignInScreen;