import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, Button} from 'react-native-elements';
import React, {useState} from "react";
import {getAuth, createUserWithEmailAndPassword} from 'firebase/auth';
import {StackScreenProps} from "@react-navigation/stack";
import UserAuthentication from "../models/UserAuthentication";
import {getInitialAuthentication} from "./SignInScreen";
import ListAPI from "../api/ListAPI";

const auth = getAuth();

const SignUpScreen: React.FC<StackScreenProps<any>> = ({ navigation }) =>  {
  const [value, setValue] = useState<UserAuthentication>(getInitialAuthentication());

  function toLoginPage() {
    navigation.navigate("sign-in");
  }
  async function signUp() {
    if (value.email === '' || value.password === '') {
      setValue({
        ...value,
        error: 'Email and password are mandatory.'
      })
      return;
    }
    try {
      await createUserWithEmailAndPassword(auth, value.email.toLocaleLowerCase(), value.password);
      await ListAPI.createDefault(value.email.toLocaleLowerCase());
      toLoginPage();
    } catch (error: any) {
      setValue({
        ...value,
        error: error.message,
      })
    }
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Signup screen!</Text>
      <View style={styles.controls}>
        <Input
          placeholder='Email'
          containerStyle={styles.control}
          value={value.email}
          onChangeText={(text) => setValue({...value, email: text})}
          leftIcon={<Icon name='envelope' size={16}/>}
        />
        <Input
          placeholder='Password'
          containerStyle={styles.control}
          value={value.password}
          onChangeText={(text) => setValue({...value, password: text})}
          secureTextEntry={true}
          leftIcon={<Icon name='key' size={16}/>}
        />
        {!!value.error && <View style={styles.error}><Text>{value.error}</Text></View>}
        <Button title="Sign up" buttonStyle={styles.control} onPress={signUp}/>
        <Button title="Return" type={"outline"} buttonStyle={styles.control} onPress={toLoginPage}/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 40,
    paddingHorizontal: 10,
    gap: 10,
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  controls: {
    flex: 1,
    gap: 10,
  },
  control: {
    width: "100%",
  },
  error: {
    marginTop: 10,
    padding: 10,
    color: '#fff',
    backgroundColor: '#D54826FF',
  }
});

export default SignUpScreen;