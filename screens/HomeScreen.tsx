import {StyleSheet, Text, View} from "react-native";
import TodoListContainer from "../components/TodoListContainer";
import React, {useState} from "react";
import {StackScreenProps} from '@react-navigation/stack';
import {Button} from "react-native-elements";
import TodoList from "../models/TodoList";
import ListAPI from "../api/ListAPI";
import {useAuthentication} from "../utils/hooks/useAuthentication";
import {useFocusEffect} from '@react-navigation/native';


const HomeScreen: React.FC<StackScreenProps<any>> = ({navigation}) => {
  const {user} = useAuthentication();
  const [lists, setLists] = useState<TodoList[]>();

  useFocusEffect(() => {
    init();
  });

  async function init() {
    if (user?.email) {
      const response = await ListAPI.findByUserEmail(user.email);
      const data: any = response.data();
      setLists(data.lists ?? []);
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        {lists === undefined && (
          <Text style={styles.subtitle}>
            Loading...
          </Text>
        )}
        {lists?.length === 0 && (
          <Text style={styles.subtitle}>
            There are no todo lists created. Press "New list" to create a new todo list.
          </Text>
        )}
        {lists?.length !== 0 &&
          lists?.map(list =>
            <TodoListContainer
              key={list.title}
              list={list}
              navigation={navigation}/>
          )}
      </View>
      <Button
        style={styles.createButton}
        title={"New list"}
        onPress={() => navigation.navigate("list-view")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
  },
  main: {
    flex: 1,
    alignItems: "flex-start",
    marginHorizontal: "auto",
    gap: 5,
  },
  subtitle: {
    padding: 15,
    fontSize: 20,
    color: "#38434D",
  },
  createButton: {
    borderRadius: 0,
  }
});

export default HomeScreen;