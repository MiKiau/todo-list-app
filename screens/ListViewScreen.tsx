import React, {useEffect, useState} from "react";
import {StackScreenProps} from "@react-navigation/stack";
import {ActivityIndicator, Alert, StyleSheet, TextInput} from "react-native";
import {View} from "react-native";
import TodoList from "../models/TodoList";
import {Button, CheckBox} from "react-native-elements";
import {useAuthentication} from "../utils/hooks/useAuthentication";
import {AntDesign, FontAwesome} from '@expo/vector-icons';
import ListAPI from "../api/ListAPI";
import ListInfo from "../models/ListInfo";
import TodoListItem from "../models/TodoListItem";
import {FieldArray, Formik} from "formik";

const ListViewScreen: React.FC<StackScreenProps<any>> = ({route, navigation}) => {
  const maybeTitle = route.params?.listTitle;

  const {user} = useAuthentication();
  const [listsInfo, setListsInfo] = useState<ListInfo>();
  const [list, setList] = useState<TodoList>();

  useEffect(() => {
    init();
  }, [user?.email]);

  async function init() {
    if (user?.email) {
      const response = await ListAPI.findByUserEmail(user.email);
      const data: any = response.data();
      const maybeList = data?.lists?.find((list: any) => list.title === maybeTitle);

      setList(getInitialTodoList(maybeList));
      setListsInfo(data);
    }
  }

  async function update(email: string, data: ListInfo) {
    ListAPI
      .update(email, data)
      .catch((err: any) => {
        alert(err.message);
      });
  }

  async function createUpdate(values: TodoList) {
    if (user?.email) {
      update(
        user.email,
        {
          email: user.email,
          lists: getNewLists(maybeTitle, listsInfo?.lists ?? [], values),
      });
      navigation.navigate("home");
    }
  }

  navigation.setOptions({
    headerRight: !Boolean(maybeTitle) ? undefined : () => (
      <AntDesign
        name="delete"
        size={24}
        color="red"
        style={{ paddingRight: 15 }}
        onPress={() => {
          createTwoButtonAlert(`Do you truly want to delete "${maybeTitle}"?`, () => {
            if (user?.email && listsInfo?.lists) {
              const newLists = removeList(listsInfo.lists, maybeTitle);
              update(
                user.email,
                {email: user.email, lists: newLists}
              );
            }
            navigation.navigate("home");
          })
        }}
      />
    )
  })

  if (!list) {
    return (
      <View style={styles.main}>
        <ActivityIndicator size="large" />
      </View>
    )
  }
  return (
    <View style={styles.main}>
      <Formik
        initialValues={list}
        onSubmit={createUpdate}>
        {({handleChange, handleBlur, setFieldValue, handleSubmit, values}) => (
          <View style={styles.container}>
            <View style={styles.inputs}>
              <TextInput
                placeholder={"- list title -"}
                style={styles.control}
                onChangeText={handleChange('title')}
                onBlur={handleBlur('title')}
                value={values.title}
              />

              <FieldArray name="items" render={(arrayHelpers) => (
                <View>
                  {values.items.length !== 0 &&
                    values.items.map((item: TodoListItem, index: number) => (
                      <View style={styles.checkbox} key={item.value + index}>
                        <CheckBox
                          checked={item.checked}
                          onPress={() => setFieldValue(`items[${index}].checked`, !item.checked)}
                          onBlur={handleBlur(`items[${index}].checked`)}
                        />
                        <TextInput
                          placeholder={'- text -'}
                          style={styles.control}
                          value={item.value}
                          onChangeText={handleChange(`items[${index}].value`)}
                          onBlur={handleBlur(`items[${index}].value`)}
                        />
                        <FontAwesome
                          name="remove"
                          size={24}
                          color="red"
                          onPress={() => arrayHelpers.remove(index)}
                        />
                      </View>
                    ))}
                  <Button
                    style={styles.button}
                    title={" Add item"}
                    onPress={() => arrayHelpers.push(getInitialTodoListItem())}
                    type={"outline"}
                    icon={
                      <AntDesign name="pluscircleo" size={18} color="black"/>
                    }
                  />
                </View>
              )}/>
            </View>
            <View style={styles.buttons}>
              <Button
                style={styles.button}
                title={maybeTitle ? "Update" : "Create new"}
                onPress={() => handleSubmit()}
              />
            </View>
          </View>
        )}
      </Formik>
    </View>
  )
}

function getInitialTodoList(maybeFound?: TodoList): TodoList {
  return {
    title: maybeFound?.title ?? "",
    items: maybeFound?.items ?? [getInitialTodoListItem()],
  }
}

function getInitialTodoListItem(): TodoListItem {
  return {
    value: "",
    checked: false,
  };
}

function getNewLists(updating: boolean, oldLists: TodoList[], values: TodoList): TodoList[] {
  if (updating) {
    const copy = oldLists.slice();
    const index = copy.findIndex((list) => list.title === values.title);
    copy[index] = values;
    return copy;
  }
  return [...oldLists, values];
}

function removeList(oldLists: TodoList[], title: string) {
  let copy = oldLists.slice();
  const index = copy.findIndex((list) => list.title === title);
  copy.splice(index, 1);
  return copy;
}

const createTwoButtonAlert = (message: string, submitAction: () => void, cancelAction?: () => void) =>
  Alert.alert(message, undefined, [
    {text: 'Cancel', onPress: cancelAction,},
    {text: 'OK', onPress: submitAction},
  ]);

const styles = StyleSheet.create({
  main: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  inputs: {
    padding: 10,
  },
  checkbox: {
    flexDirection: "row",
    margin: 0,
    padding: 0,
    alignItems: 'center',
  },
  control: {
    margin: 0,
    padding: 0,
    flexDirection: "row",
    flexGrow: 1,
  },
  buttons: {
    gap: 10,
  },
  button: {
    width: "100%",
  },
  addListItem: {
    width: "100%",
    paddingHorizontal: 10,
  },
  error: {
    marginTop: 10,
    padding: 10,
    color: '#fff',
    backgroundColor: '#D54826FF',
  }
});

export default ListViewScreen;