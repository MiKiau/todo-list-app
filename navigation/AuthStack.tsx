import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SignInScreen from '../screens/SignInScreen';
import SignOutScreen from '../screens/SignUpScreen';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={"sign-in"}>
        <Stack.Screen name="sign-in" component={SignInScreen} options={{ headerShown: false }} />
        <Stack.Screen name="sign-up" component={SignOutScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}