import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {AntDesign} from "@expo/vector-icons";
import {getAuth, signOut} from 'firebase/auth';
import HomeScreen from '../screens/HomeScreen';
import ListViewScreen from '../screens/ListViewScreen';

const Stack = createStackNavigator();
const auth = getAuth();

export default function UserStack() {
  async function logout() {
    await signOut(auth);
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={"home"}>
        <Stack.Screen
          name="home"
          component={HomeScreen}
          options={{
            headerRight: () => (
              <AntDesign
                name="logout"
                size={24}
                color="black"
                onPress={logout}
                style={{
                  paddingRight: 15,
                }}
              />
            ),
          }}
        />
        <Stack.Screen name="list-view" component={ListViewScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}