import "./../config/firebase";
import {collection, doc, getDoc, setDoc, getFirestore, Firestore} from "firebase/firestore";
import ListInfo from "../models/ListInfo";

const db: Firestore = getFirestore();
const collectionBaseName = "lists";

const ListAPI = {
  createDefault: (email: string) => setDoc(doc(collection(db, collectionBaseName), email), {email: email, lists: []} ),
  update: (email: string, data: ListInfo) => setDoc(doc(collection(db, collectionBaseName), email), data),
  findByUserEmail: (email: string) => getDoc(doc(db, collectionBaseName, email)),
};

export default ListAPI;