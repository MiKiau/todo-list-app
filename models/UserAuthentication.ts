interface UserAuthentication {
  email: string;
  password: string;
  error: string;
}

export default UserAuthentication;