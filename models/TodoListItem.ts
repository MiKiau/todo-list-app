interface TodoListItem {
  value: string;
  checked: boolean;
}

export default TodoListItem;