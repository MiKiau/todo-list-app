import TodoList from "./TodoList";

interface ListInfo {
  email: string;
  lists: TodoList[];
}

export default ListInfo;