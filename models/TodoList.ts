import TodoListItem from "./TodoListItem";

interface TodoList {
  title: string;
  items: TodoListItem[];
}

export default TodoList;